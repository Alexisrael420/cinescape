package com.example.cinescape.Models;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinescape.Interfases.PostService;
import com.example.cinescape.R;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Info extends AppCompatActivity {

    TextView txtNombre, txtDesc, txtCorreo, txtNum, txtInfo;
    Button btnRentar, btnLlamar, btnVolver;
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);


        txtNombre = (TextView)findViewById(R.id.txtNombre);
        txtDesc = (TextView)findViewById(R.id.txtDesc);
        txtCorreo = (TextView)findViewById(R.id.txtCorreo);
        txtNum = (TextView)findViewById(R.id.txtNum);
        txtInfo = (TextView)findViewById(R.id.txtInfo);
        btnRentar = (Button)findViewById(R.id.btnRentar);
        btnLlamar = (Button)findViewById(R.id.btnLlamar);
        btnVolver = (Button)findViewById(R.id.btnVolver);

        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");
        resultado(nombre);

        btnRentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try (GestorBD bd = new GestorBD(getApplicationContext())) {
                    // guardar datos
                    bd.agregarPelicula(txtNombre.getText().toString(), txtDesc.getText().toString(), txtCorreo.getText().toString(), txtNum.getText().toString());
                    Toast.makeText(getApplicationContext(), "EXITO EN RENTA", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Info.this, Renta.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = txtNum.getText().toString();
                if (tel != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionAntigua(tel);
                    }
                }
            }

            private void versiomNueva(){

            }
            private  void versionAntigua(String tel){
                Intent intentLlamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+tel));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamar);
                }else{
                    Toast.makeText(Info.this, "Active el permiso de llamadas", Toast.LENGTH_SHORT);
                }
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void resultado(String q){
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostService postService = retrofit.create(PostService.class);
        Call<List<Post>> call = postService.find(q);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> postsList = response.body();
                for (Post p: postsList){
                    String nombre = p.getName();
                    String correo = p.getEmail();
                    String descrpcion = p.getBody();
                    String telefono = "55-0000-0000";
                    txtNombre.append(nombre);
                    txtCorreo.append(correo);
                    txtDesc.append(descrpcion);
                    txtNum.append(telefono);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                txtInfo.setText(t.getMessage());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){

                    if (result == PackageManager.PERMISSION_GRANTED){
                        String numTel = txtNum.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numTel));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else {
                        Toast.makeText(this, "No haz autorizado el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}