package com.example.cinescape.Models;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinescape.Interfases.PostService;
import com.example.cinescape.Interfases.PostServiceLista;
import com.example.cinescape.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Catalogo extends AppCompatActivity {

    EditText edtPalabra;
    Button btnBuscar, btnRefresh, btnPeliculas;
    TextView txtResultado;
    ListView lvCatalogo;
    ArrayList<String> arrayNames;
    ArrayAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_catalogo);


        arrayNames = new ArrayList<String>();
        edtPalabra = (EditText)findViewById(R.id.edtBuscar);
        btnBuscar = (Button)findViewById(R.id.btnBuscar);
        btnRefresh = (Button)findViewById(R.id.btnRefresh);
        btnPeliculas = (Button)findViewById(R.id.btnPeliculas);
        txtResultado = (TextView)findViewById(R.id.txtResultado);
        lvCatalogo = (ListView) findViewById(R.id.lvCatalogo);
        listaAccion();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayNames);

        //enviar nombre a detalle para hacer consulta
        lvCatalogo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                Intent intent = new Intent(Catalogo.this, Info.class);
                intent.putExtra("TITULO", arrayNames.get(position));
                startActivity(intent);
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayNames.clear();
                resultado(edtPalabra.getText().toString());
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayNames.clear();
                listaAccion();
            }
        });

        btnPeliculas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Catalogo.this, Renta.class);
                startActivity(intent);
            }
        });
    }

    public void resultado(String q){
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostService postService = retrofit.create(PostService.class);
        Call<List<Post>> call = postService.find(q);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> postsList = response.body();
                for (Post p: postsList){
                    String n = p.getName();
                    arrayNames.add(n);
                }
                lvCatalogo.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                txtResultado.setText(t.getMessage());
            }
        });
    }

    public  void listaAccion(){
        for (int i = 1; i <= 50; i++ ){
            lista(i+"");
        }
    }

    public void lista(String q){
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostServiceLista postServiceLista = retrofit.create(PostServiceLista.class);
        Call<List<Post>> call = postServiceLista.find(q);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> postsList = response.body();
                for (Post p: postsList){
                    String n = p.getName();
                    arrayNames.add(n);
                }
                lvCatalogo.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                txtResultado.setText(t.getMessage());
            }
        });
    }
}